#!/bin/sh -e

$PKG_CMD new A 1
$PKG_CMD b A
$PKG_CMD i A

tar_ball="$($PKG_CMD export A)"
[ -f "$tar_ball" ]
$PKG_CMD r A
$PKG_CMD import "$tar_ball"
$PKG_CMD list A | grep 1

$PKG_CMD r A

renamed_tarball=arbitrary_name.tar
mv "$tar_ball" "$renamed_tarball"
$PKG_CMD import "$renamed_tarball"
$PKG_CMD list A | grep 1

$PKG_CMD r A

gzip "$renamed_tarball"

$PKG_CMD import "$renamed_tarball.gz"
$PKG_CMD list A | grep 1
