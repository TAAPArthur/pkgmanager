#!/bin/sh -e

create_factor_packages 12
$PKG_CMD b 12
$PKG_CMD i 12

lastCount=0
$PKG_CMD tree 12 | tr -c -d "[:space:]" | tr "\t" ,| while read -r line; do
    count=${#line}
    [ "$lastCount" -ge "$((count - 1))" ]
    lastCount="$count"
    [ "$lastCount" -ne 0 ] || lastCount=1
done
