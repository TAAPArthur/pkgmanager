#!/bin/sh -e

$PKG_CMD new A

BUILD_DIR="$PWD/tmp"
mkdir "$BUILD_DIR"
{
    echo '[ "$PWD" = "'"$BUILD_DIR"'"/1 ]'
    echo 'touch file'
    echo 'mv * $1/'
} >> A/build

cd "$BUILD_DIR"
mkdir 1
touch 1/test_file
PKGMAN_BUILD_DIR=. $PKG_CMD b A
$PKG_CMD i A

[ -e "$PKGMAN_ROOT/file" ]
[ ! -e "1/test_file" ]
[ -e "$PKGMAN_ROOT/test_file" ]
