#!/bin/sh

$PKG_CMD new A
$PKG_CMD new B
$PKG_CMD new C

export FILE_NAME="${ARG:-file}"

echo 'touch "$1/$FILE_NAME"' | tee A/build B/build C/build >/dev/null

$PKG_CMD b A B C

$PKG_CMD i B A C
[ -e "$PKGMAN_ROOT/$FILE_NAME" ]
[ "$($PKG_CMD owns "/$FILE_NAME")" = B ]

$PKG_CMD manifest A C | grep -Fx "/$FILE_NAME" && exit 1
$PKG_CMD manifest B | grep -Fx "/$FILE_NAME"

[ "$($PKG_CMD owns "/$FILE_NAME")" = B ]

$PKG_CMD a | grep A
$PKG_CMD a | grep B && exit 1
$PKG_CMD a | grep C
$PKG_CMD a | grep A | $PKG_CMD a -

[ "$($PKG_CMD owns "/$FILE_NAME")" = A ]
$PKG_CMD manifest A B C
$PKG_CMD manifest B C | grep -Fx "/$FILE_NAME" && exit 1

$PKG_CMD manifest A | grep -Fx "/$FILE_NAME"
$PKG_CMD a | grep A && exit 1
[ "$($PKG_CMD owns "/$FILE_NAME")" = A ]
