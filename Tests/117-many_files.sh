#!/bin/sh

$PKG_CMD new A

NUM=256

cat - >> A/build <<EOF
for i in \$(seq 1 "$NUM"); do
    touch \$1/\$i
done
EOF

cp -R A B

$PKG_CMD b A
$PKG_CMD i A

# shellcheck disable=2266
seq 1 "$NUM" | sed "s/^/\//" | pkgman owns - | grep ^A | [ "$(wc -l)" -eq "$NUM" ]

$PKG_CMD b B
$PKG_CMD i B

# shellcheck disable=2266
seq 1 "$NUM" | sed "s/^/\//" | pkgman owns - | grep ^A | [ "$(wc -l)" -eq "$NUM" ]

$PKG_CMD a | grep ^B | $PKG_CMD a -

# shellcheck disable=2266
seq 1 "$NUM" | sed "s/^/\//" | pkgman owns - | grep ^B | [ "$(wc -l)" -eq "$NUM" ]

$PKG_CMD r A

# shellcheck disable=2266
seq 1 "$NUM" | sed "s/^/\//" | pkgman owns - | grep ^B | [ "$(wc -l)" -eq "$NUM" ]
