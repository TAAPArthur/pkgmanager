#!/bin/sh -e

git_commit() {
    git -c user.name='Test' -c user.email='user@test.com' commit -a -m"$1"
}
git_add_file_and_commit() {
    mkdir "$1"
    (cd "$1"; git init; touch file; git add .; git_commit "Init"; echo "$1" > file; git_commit msg ) >/dev/null 2>&1
    ( cd "$1"; git rev-parse HEAD )
}
git_add_file_and_commit git_repo
git_add_file_and_commit git_repo/A
hashB=$(git_add_file_and_commit git_repo/B)
git_add_file_and_commit git_repo/C


$PKG_CMD new A git
{
    echo "git+file://$PWD/git_repo"
    echo "git+file://$PWD/git_repo/A@HEAD" A
    echo "git+file://$PWD/git_repo/B@$hashB" B
    echo "git+file://$PWD/git_repo/C" C
} > A/sources

echo 'mv * "$1/"'   > A/build
set -x
for _ in 1 2; do
    $PKG_CMD b A
    $PKG_CMD i A

    [ -e "$PKGMAN_ROOT/file" ]
    [ -e "$PKGMAN_ROOT/A/file" ]
    [ -e "$PKGMAN_ROOT/B/file" ]
    [ -e "$PKGMAN_ROOT/C/file" ]
    echo --------------
done
