#!/bin/sh -e

git_commit() {
    git -c user.name='Test' -c user.email='user@test.com' commit -a -m"$1"
}
install_and_verify() {
    pkg=$1
    hash_str=$2
    $PKG_CMD b "$pkg"
    $PKG_CMD i "$pkg"
    $PKG_CMD list "$pkg" | grep "$hash_str"

    [ -z "$($PKG_CMD outdated --git)" ]
}
mkdir git_repo
( cd git_repo; git init; touch file; git add .; git_commit "Init")
hashBase="$(cd git_repo; git rev-parse HEAD)"

$PKG_CMD new A git "git+file://$PWD/git_repo"

install_and_verify A "$hashBase"

( cd git_repo; echo "A" > file; git_commit "A")

hashA="$(cd git_repo; git rev-parse HEAD)"

[ -z "$($PKG_CMD outdated)" ]
[ -n "$($PKG_CMD outdated --git)" ]
echo y | $PKG_CMD update --git
$PKG_CMD list A | grep "$hashA"

[ -z "$($PKG_CMD outdated)" ]
[ -z "$($PKG_CMD outdated --git)" ]

( cd git_repo; git checkout -B "new_branch"; echo "B" > file; git_commit "B"; )
hashB="$(cd git_repo; git rev-parse HEAD)"
( cd git_repo; git checkout master )

[ -z "$($PKG_CMD outdated)" ]
[ -z "$($PKG_CMD outdated --git)" ]

$PKG_CMD new B git "git+file://$PWD/git_repo@$hashB"
echo 'read -r var <file; [ "$var" = B ]' >> B/build
install_and_verify B "$hashB"

[ -z "$($PKG_CMD outdated --git)" ]

( cd git_repo; git checkout "new_branch"; echo "C" > file; git_commit "C"; )
hashC="$(cd git_repo; git rev-parse HEAD)"
( cd git_repo; git checkout master )
install_and_verify B "$hashB"

$PKG_CMD new C git "git+file://$PWD/git_repo@new_branch"
echo 'read -r var <file; [ "$var" = C ]' >> C/build

install_and_verify C "$hashC"

install_and_verify C "$hashC"
