#!/bin/sh -e


NAME50=$(seq -w -s "" 1 50)
NAME127=$(seq -w -s "" 1 63)
NAME128=$(seq -w -s "" 1 64)
NAME254="${NAME127}${NAME127}"

$PKG_CMD new A
{
    echo 'cd "$1"'
    echo touch "$NAME50"
    echo touch "$NAME254"
} >> A/build

$PKG_CMD b A

tar -tf "$($PKG_CMD export A)"
$PKG_CMD i A

tree "$PKGMAN_ROOT/"
[ -f "$PKGMAN_ROOT/$NAME50" ]
[ -f "$PKGMAN_ROOT/$NAME254" ]

$PKG_CMD new "$NAME128"

$PKG_CMD b "$NAME128"
$PKG_CMD i "$NAME128"
$PKG_CMD list "$NAME128"
