#!/bin/sh -e

create_factor_packages 4
$PKG_CMD b 4
$PKG_CMD list
tar_ball="$($PKG_CMD export 4)"
[ -f "$tar_ball" ]
$PKG_CMD r 4 2 1 0
$PKG_CMD import "$tar_ball"

$PKG_CMD list 4
$PKG_CMD list 2
$PKG_CMD list 1
! $PKG_CMD list 0 || exit 1
