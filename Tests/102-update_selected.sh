#!/bin/sh


export PKGMAN_FORCE=1
$PKG_CMD update
set -x

for pkg in A B C; do
    $PKG_CMD new "$pkg"
    $PKG_CMD b "$pkg"
    $PKG_CMD i "$pkg"
done

for file in */version; do
    [ -r "$file" ]
    echo "1 2" > "$file"
done
pkgman update A C

$PKG_CMD list A | grep "1 2"
$PKG_CMD list B | grep "1 1"
$PKG_CMD list C | grep "1 2"

for file in */version; do
    [ -r "$file" ]
    echo "1 3" > "$file"
done
pkgman update unknown_pkg A C

$PKG_CMD list A | grep "1 3"
$PKG_CMD list B | grep "1 1"
$PKG_CMD list C | grep "1 3"

[ -z "$(PKG_CMD outdated B A C)" ]
