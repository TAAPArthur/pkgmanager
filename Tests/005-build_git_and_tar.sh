#!/bin/sh -e

git_commit() {
    git -c user.name='Test' -c user.email='user@test.com' commit -a -m"$1"
}

mkdir -p pkg/A/B/C/D
mkdir -p pkg/empty
touch pkg/b pkg/c

tar -c pkg > pkg.tar

mkdir git_repo
(cd git_repo; git init; mkdir empty; touch empty/file; git add .; git_commit "Init" ) >/dev/null 2>&1

$PKG_CMD new A 1 "file://$PWD/pkg.tar" "git+file://$PWD/git_repo"
$PKG_CMD new B 1 "git+file://$PWD/git_repo" "file://$PWD/pkg.tar"

$PKG_CMD b A
! $PKG_CMD b B
