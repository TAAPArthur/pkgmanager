#!/bin/sh

$PKG_CMD new A
$PKG_CMD new B

echo 'touch "$1/file"' > A/build
echo 'touch "$1/file" "$1/file2"' > B/build

$PKG_CMD b A B
$PKG_CMD i A B

[ "$($PKG_CMD owns /file)" = A ]
echo A /file | $PKG_CMD a -
[ "$($PKG_CMD owns /file)" = A ]

echo B /file | $PKG_CMD a -
[ "$($PKG_CMD owns /file)" = B ]
[ "$($PKG_CMD owns /file2)" = B ]

echo A /file2 | $PKG_CMD a - || true
[ "$($PKG_CMD owns /file2)" = B ]

echo A /file_that_doesnt_exit | $PKG_CMD a -
