#!/bin/sh -e

FILE=$PWD/file
FILE2=$PWD/file2
touch "$FILE" "$FILE2"
$PKG_CMD new A 1 "file://$FILE dir1" "file://$FILE2 dir2"
cat - >> A/build <<EOF
[ -d dir1 ]
[ -d dir2 ]
[ -e dir1/file ]
[ -e dir2/file2 ]
EOF

$PKG_CMD b A
