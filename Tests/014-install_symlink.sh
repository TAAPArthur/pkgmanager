#!/bin/sh -e

mkdir "$PKGMAN_ROOT/etc"
touch "$PKGMAN_ROOT/etc/C" "$PKGMAN_ROOT/etc/D"
$PKG_CMD new A
{
echo 'mkdir -p "$1/etc"'
echo 'ln -s A "$1/etc/B"'
echo 'ln -s C "$1/etc/D"'
echo 'mkdir "$1/etc/DIR"'
echo 'ln -s DIR "$1/etc/dir"'
} >> A/build

verify() {
    [ -h "$PKGMAN_ROOT/etc/B" ]
    [ -h "$PKGMAN_ROOT/etc/D" ]
    [ -h "$PKGMAN_ROOT/etc/dir" ]

    [ -d "$PKGMAN_ROOT/etc/dir" ]
}
$PKG_CMD b A
$PKG_CMD i A
verify
[ ! -e "$PKGMAN_ROOT/etc/A" ]
$PKG_CMD i A
verify

touch "$PKGMAN_ROOT/etc/A"

$PKG_CMD i A

verify
