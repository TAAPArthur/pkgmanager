#!/bin/sh -e

$PKG_CMD new A
$PKG_CMD new B

echo echo hi >> "A/build"
ln -sf ../A/build B/build

$PKG_CMD b A B
$PKG_CMD i A B
$PKG_CMD list A B
