#!/bin/sh

$PKG_CMD new A
echo 'echo 0 > "$1/a"'  >> A/build
$PKG_CMD b A
$PKG_CMD i A

exec 3< "$PKGMAN_ROOT/a"

echo 'echo 1 > "$1/a"'  > A/build

set -x
$PKG_CMD b A
$PKG_CMD i A

read -r value <&3
[ "$value" -eq 0 ]
