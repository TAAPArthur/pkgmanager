#!/bin/sh


verify_perms() {
    perm=$1
    shift
    for file; do
        echo "------------------------"
        echo "$file"
        stat "$PKGMAN_ROOT/$file" | grep Access
        ls -alt "$PKGMAN_ROOT/$file"
        stat "$PKGMAN_ROOT/$file" | grep Access | grep -q "$perm"
    done
}

$PKG_CMD new A
echo 'cd $1; touch a b c; chmod 700 a b c; chmod u+s c; ls -alt; stat c' >> A/build
$PKG_CMD b A
$PKG_CMD i A
verify_perms 0700 a b
verify_perms 4700 c
$PKG_CMD r A
$PKG_CMD import "$($PKG_CMD export A)"

verify_perms 0700 a b
verify_perms 4700 c
