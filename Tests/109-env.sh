#!/bin/sh

$PKG_CMD new A

echo "FOO=BAR" >> A/env
echo "BAR=FOO" >> A/env
echo '[ "$FOO" = BAR ]' >> A/build
echo '[ "$BAR" = FOO ]' >> A/build

$PKG_CMD b A

$PKG_CMD new B

echo "B=~/.config" >> B/env
echo 'set -x; echo $B' >> B/build
echo '[ "$B" = $HOME/.config ]' >> B/build
$PKG_CMD b B
