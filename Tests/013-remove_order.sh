#!/bin/sh -e

$PKG_CMD new A
$PKG_CMD new B
$PKG_CMD new C
$PKG_CMD new A2
echo "A" > B/depends
echo "B" > C/depends
echo "C" > A2/depends
mkdir temp
cd temp
$PKG_CMD b B C A A2
$PKG_CMD i A B A2 C
$PKG_CMD r A C A2 B
