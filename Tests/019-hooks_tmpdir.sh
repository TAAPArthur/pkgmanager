#!/bin/sh

mkdir hooks.d
export PKGMAN_HOOK_PATH="$PKGMAN_HOOK_PATH:$PWD/hooks.d"

OLDTMPDIR=$TMPDIR
TMPDIR=/dev/null

cat >>hooks.d/hook.sh <<EOF
[ "\$PKG" = A ]
case "\$TYPE" in
    pre-create-wd) TMPDIR=$OLDTMPDIR  ;;
esac
EOF

$PKG_CMD new A
$PKG_CMD b A
$PKG_CMD i A

