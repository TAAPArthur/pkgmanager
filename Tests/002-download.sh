#!/bin/sh -e

mkdir temp
export PKGMAN_SOURCE_CACHE_DIR="$PWD/dir/temp"
SRC_DIR=$PWD/srcs
FILE=$SRC_DIR/file
mkdir "$SRC_DIR"
touch "$FILE"
$PKG_CMD new A 1 "file://$FILE"
$PKG_CMD dowload A

[ -d "$PKGMAN_SOURCE_CACHE_DIR" ]
[ -e "$PKGMAN_SOURCE_CACHE_DIR/A/$(basename "$FILE")" ]
[ ! -e "file" ]

FILE2="$SRC_DIR/fake_file"
touch "$FILE2"
$PKG_CMD new B 1 "file://$FILE" "file://$FILE2"
$PKG_CMD dowload B

[ -d "$PKGMAN_SOURCE_CACHE_DIR/B" ]
[ -e "$PKGMAN_SOURCE_CACHE_DIR/B/$(basename "$FILE")" ]
[ -e "$PKGMAN_SOURCE_CACHE_DIR/B/$(basename "$FILE2")" ]
[ ! -e "fake_file" ]
