#!/bin/sh

$PKG_CMD new A 1

just_update() {
    echo "$1 $2" > A/version
}
update_and_build() {
    just_update "$@"
    echo "touch \$1/$1_$2" > A/build
    $PKG_CMD b A

}

verify() {
    [ -e "$PKGMAN_ROOT/$1_$2" ]
    $PKG_CMD list A | grep "$1 $2"
}

update_and_build 1 1

update_and_build 1 2

$PKG_CMD i A
verify 1 2

update_and_build 1 3
verify 1 2
$PKG_CMD i A
verify 1 3

# We should build what ever the current version is and not what the latest or last built is
just_update 1 1
verify 1 3
$PKG_CMD i A
verify 1 1

update_and_build 2 1
$PKG_CMD i A
verify 2 1
