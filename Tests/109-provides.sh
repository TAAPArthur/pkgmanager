#!/bin/sh

$PKG_CMD new A
$PKG_CMD new a
$PKG_CMD new b Version10

mkdir A/provides
mkdir b/provides

echo '2 3' > A/provides/a
echo 'exit 1' > a/build
echo 'a' > b/depends
touch b/provides/B

$PKG_CMD i A
$PKG_CMD list A a

$PKG_CMD i b

$PKG_CMD list b B
$PKG_CMD list B | grep Version10
$PKG_CMD list b | grep Version10
