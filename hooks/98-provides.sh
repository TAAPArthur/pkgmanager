#!/bin/sh

case "$TYPE" in
    post-build)
        METADATA=$(PKGMAN_ROOT="$PWD" pkgmanager get-metadata-install-dir "$PKG")
        if [ -d "$METADATA/provides" ]; then
            for file in "$METADATA/provides/"*; do
                provides_dir="$PWD/$(PKGMAN_ROOT="" pkgmanager get-metadata-install-dir "$(basename "$file")")"
                mkdir -p "$provides_dir"
                [ -s "$file" ] || file="$METADATA/version"
                cp "$file" "$provides_dir/version"
            done
        fi
        ;;
esac
