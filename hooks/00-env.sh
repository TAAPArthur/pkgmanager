#!/bin/sh

if [ "$TYPE" = begin-build ] && [ -r "$PKG_PATH/env" ]; then
    while IFS="=" read -r VAR VALUE; do
        if [  \~/"${VALUE#\~/}" = "${VALUE}" ]; then
            VALUE="$HOME/${VALUE#\~/}"
        fi
        read -r "${VAR?}" <<EOF
$VALUE
EOF
        export "${VAR?}"
    done < "$PKG_PATH/env"
fi
