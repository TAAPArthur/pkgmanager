#!/bin/sh -e

case "$TYPE" in
    pre-install)
        pkg_gen_manifest | { if [ -r "$3" ]; then comm -23 - "$3"; else cat -; fi; } | while read -r path; do
            # If package is owned by another package
            if [ -e "$PKGMAN_ROOT/$path" ] && [ ! -d "$PKGMAN_ROOT/$path" ] ; then
               echo "$path"
            fi
        done | pkgmanager owns - | PKGMAN_ROOT="$PWD" pkgmanager swap-conflict - "$PKG"
        # if you don't want conflict management either
        # (0) Don't use this hook and silently override existing files
        # (1) Exit with non-zero status to abort the install
        # (2) Uncomment the following to remove the file (and remove the previous non-comment line)
        # done | pkgmanager owns - | while read -r owner path; do
        #     if [ "$PKG" != "$owner" ]; then
        #         rm "$path"
        #     fi
        # done
        ;;
esac
